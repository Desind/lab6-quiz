package com.company;

import java.util.Random;

import static java.lang.Integer.compare;

public class QuizImpl implements Quiz{
    private int quizValue;

    public QuizImpl(){
        Random r = new Random();
        this.quizValue = r.nextInt((MAX_VALUE - MIN_VALUE) + 1) + MIN_VALUE;
        System.out.println(this.quizValue);
    }

    @Override
    public void isCorrectValue(int value) throws ParamTooLarge, ParamTooSmall {
        int compareAnswer;
        compareAnswer = compare(this.quizValue,value);
        if(compareAnswer>0){
            throw new ParamTooSmall();
        }else if(compareAnswer<0){
            throw new ParamTooLarge();
        }
    }
}
