package com.company;

public class Main {

    public static void main(String[] args) {
        Quiz quiz = new QuizImpl();
        int bottomOfRange = Quiz.MIN_VALUE;
        int topOfRange = Quiz.MAX_VALUE;
        int digit = (topOfRange+bottomOfRange)/2;
        for(int counter = 1;;counter++){
            try {
                quiz.isCorrectValue(digit);
                System.out.println("Poprawna liczba to: "+ digit);
                System.out.println("Liczba krokow: "+counter);
                break;
            } catch (Quiz.ParamTooLarge paramTooLarge) {
                topOfRange = digit;
                digit = (topOfRange+bottomOfRange)/2;
                System.out.println("Za duza: " + digit);
            } catch (Quiz.ParamTooSmall paramTooSmall) {
                bottomOfRange = digit;
                digit = (topOfRange+bottomOfRange)/2;
                System.out.println("za mala: "+ digit);
            }
        }
    }
}
